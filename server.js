const express = require("express");
const http = require("http");
const redis = require("redis");
const dotenv = require("dotenv");
const result = dotenv.config();
const { Client } = require("pg");

const app = express();

let client = redis.createClient({ port: "6379", host: "redis-server" });

app.get("/", function (req, res, next) {
  client.incr("counter", function (err, counter) {
    if (err) return next(err);
    res.send("This page has been viewed " + counter + " times!");
  });
});
app.get("/db", async function (req, res, next) {
  const client = new Client({
    user: process.env.POSTGRES_USER,
    host: "node2-db",
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
  });
  await client.connect();
  const resp = await client.query("SELECT $1::text as message", [
    "Hello world!",
  ]);
  console.log(resp.rows[0].message); // Hello world!
  await client.end();
  res.send("ok");
});

http.createServer(app).listen(process.env.PORT || 442, () => {
  console.log("server is listening on ", process.env.PORT);
});
