FROM node:12

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . /usr/src/app

#RUN npm run build

EXPOSE 442

#RUN npm run start
CMD ["node", "./server.js"]
